# NUwUs Web Interface Build

## Description
Ce repo sert à déployer l'interface web de gestion des événements. Il ne doit donc pas être modifié à la main. Le repo principal du projet peut se trouver [ici](https://gitlab.com/UwU-Ense3/nuwus-web-interface).

## Utilisation
Pour déployer une nouvelle version : depuis le [projet principal](https://gitlab.com/UwU-Ense3/nuwus-web-interface) en Flutter, exécutez la commande suivante afin de build une release :

```
flutter build web --release
```

Le contenu du dossier `build/web` contient le nouveau build. Copiez le contenu de ce dossier dans le dossier `web` de ce repo, commit et push.

Sur le serveur, faites un `git pull` pour récupérer les modifications. Relancez ensuite le container :

```
sudo docker-compose up --build -d
```

